package dodopriester.pixeliser.main;

import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;

import javax.imageio.ImageIO;

public class Main {

	public static String fileName = "";
	
	public static void main(String[] args) {
	while(true) {
	System.out.println("Bitte gebe den Dateipfad ein: ");
	BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
    
	try {
		fileName = br.readLine();
	} catch (IOException e) {
		e.printStackTrace();
	}
	
    try {
		pixeliser();
	} catch (IOException e) {
		System.err.println("Etwas lief schief! " + e.getMessage());
	}
	}
	}

	private static void pixeliser() throws IOException {
		BufferedImage img = ImageIO.read(new File(fileName));
		BufferedImage newImg = new BufferedImage(img.getWidth(), img.getHeight(), img.getType());
		for(int y = 0; y < img.getHeight(); y = y+2) {
			for(int x = 0; x < img.getWidth(); x = x+2) {
				int i = img.getRGB(x, y);
				newImg.setRGB(x, y, i);
				try {
				if(y < img.getHeight()-1) {
				if(x != 0)
				newImg.setRGB(x-1, y+1, i);
			
				newImg.setRGB(x, y+1, i);
				if(x < img.getWidth()-1) {
				newImg.setRGB(x+1, y+1, i);
				}
				
				
				if(x != 0)
				newImg.setRGB(x-1, y, i);
				if(x < img.getWidth()-1)
				newImg.setRGB(x+1, y, i);
				
				if(y != 0) {
				if(x != 0)
				newImg.setRGB(x-1, y-1, i);
				newImg.setRGB(x, y-1, i);
				if(x < img.getWidth()-1)
				newImg.setRGB(x+1, y-1, i);
				}
				}
				
				} catch(ArrayIndexOutOfBoundsException e) {
					System.out.println(e.getMessage() + " bei X: " + x + " Y: " + y);
				}
			}
			
		}
		
		File outputFile = new File("Pixel_" + fileName);
		ImageIO.write(newImg, "png", outputFile);
		
		System.out.println("Datei wurde bei " + outputFile.getAbsolutePath() + " gespeichert. ");
	}

}
